export class Job {
  id: number;
  name: string;
  status: string;
  people: number[];
  tags: string[];
  dateCreated: Date;
  dateUpdated: Date;
}
