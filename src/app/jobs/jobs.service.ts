import { Injectable } from "@angular/core";
import { Job } from "./jobs";
import { JOBS } from "./mock-jobs";
import { MessageService } from "../message.service";

import { Observable, of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class JobsService {
  constructor(private messageService: MessageService) {}

  getJobs(): Observable<Job[]> {
    // TODO: send the message _after_ fetching the tests
    this.messageService.add("PersonService: fetched tests");
    return of(JOBS);
  }
}
