import { Component, OnInit, ViewChild } from "@angular/core";
import { Job } from "./jobs";
import { JobsService } from "./jobs.service";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";

@Component({
  selector: "app-jobs",
  templateUrl: "./jobs.component.html",
  styleUrls: ["./jobs.component.scss"],
})
export class JobsComponent implements OnInit {
  jobs: Job[];

  displayedColumns: string[] = ["id", "name", "status", "dateCreated", "view", "complete", "remove"];
  dataSource: MatTableDataSource<Job>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private jobService: JobsService) {
    this.getJobs();
    this.dataSource = new MatTableDataSource(this.jobs);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getJobs(): void {
    this.jobService.getJobs().subscribe(jobs => (this.jobs = jobs));
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
