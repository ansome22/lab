import { Job } from "./jobs";

export const JOBS: Job[] = [
  { id: 1, name: "Job 1", status: "Approved", people: [1, 3], tags: ["Percentage", "Cold"], dateCreated: new Date("06/02/2019"), dateUpdated: new Date() },
  { id: 2, name: "Job 2", status: "Waiting", people: [5, 2], tags: ["Percentage", "Hot"], dateCreated: new Date("06/02/2019"), dateUpdated: new Date() },
  { id: 3, name: "Job 3", status: "Processing", people: [2, 4], tags: ["Percentage", "Cold", "Graph"], dateCreated: new Date("12/01/2019"), dateUpdated: new Date() },
  { id: 4, name: "Job 4", status: "Completed", people: [2, 4], tags: ["Percentage", "Cold", "Waiting"], dateCreated: new Date("12/01/2019"), dateUpdated: new Date() },
];
