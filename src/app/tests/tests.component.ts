import { Component, OnInit, ViewChild } from "@angular/core";
import { Test } from "./test";
import { TestsService } from "./tests.service";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";

@Component({
  selector: "app-tests",
  templateUrl: "./tests.component.html",
  styleUrls: ["./tests.component.scss"],
})
export class TestsComponent implements OnInit {
  tests: Test[];

  displayedColumns: string[] = ["id", "name", "status", "dateCreated", "view", "complete", "remove"];
  dataSource: MatTableDataSource<Test>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private testService: TestsService) {
    this.getTests();
    this.dataSource = new MatTableDataSource(this.tests);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getTests(): void {
    this.testService.getTests().subscribe(tests => (this.tests = tests));
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
