import { Injectable } from "@angular/core";
import { Test } from "./test";
import { TESTS } from "./mock-test";
import { MessageService } from "../message.service";

import { Observable, of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class TestsService {
  constructor(private messageService: MessageService) {}

  getTests(): Observable<Test[]> {
    // TODO: send the message _after_ fetching the tests
    this.messageService.add("PersonService: fetched tests");
    return of(TESTS);
  }
}
