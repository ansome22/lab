import { Test } from "./test";

export const TESTS: Test[] = [
  { id: 1, name: "Test 1", status: "Approved", people: [1, 3], tags: ["Percentage", "Cold"], dateCreated: new Date("4/01/2019"), dateUpdated: new Date() },
  { id: 2, name: "Test 2", status: "Processing", people: [5, 2], tags: ["Percentage", "Hot"], dateCreated: new Date("3/01/2019"), dateUpdated: new Date() },
  { id: 3, name: "Test W Graph", status: "Completed", people: [2, 4], tags: ["Percentage", "Cold", "Graph"], dateCreated: new Date("06/02/2019"), dateUpdated: new Date() },
  { id: 4, name: "Test 4", status: "Waiting", people: [2, 4], tags: ["Percentage", "Cold", "Waiting"], dateCreated: new Date("06/02/2019"), dateUpdated: new Date() },
];
