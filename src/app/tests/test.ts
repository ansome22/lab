export class Test {
  id: number;
  name: string;
  status: string;
  people: number[];
  tags: string[];
  dateCreated: Date;
  dateUpdated: Date;
  results?: Result;
}

export class Result {
  name: string;
  xTag: string;
  yTag: string;
  graph: ResultGraph[];
}

export class ResultGraph {
  x: string;
  y: string;
  vale: string;
}
