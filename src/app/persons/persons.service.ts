import { Injectable } from "@angular/core";
import { Person } from "./persons";
import { PERSONS } from "./mock-persons";
import { MessageService } from "../message.service";

import { Observable, of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PersonsService {
  constructor(private messageService: MessageService) {}

  getPersons(): Observable<Person[]> {
    // TODO: send the message _after_ fetching the persons
    this.messageService.add("PersonService: fetched persons");
    return of(PERSONS);
  }

  getPerson(id: number): Observable<Person> {
    // TODO: send the message _after_ fetching the persons
    this.messageService.add(`PersonService: fetched person id=${id}`);
    return of(PERSONS.find(person => person.id === id));
  }
}
