import { Person } from "./persons";

export const PERSONS: Person[] = [
  { id: 1, fname: "Mr.", lname: "Nice", position: "Admin" },
  { id: 2, fname: "Narco", lname: "Nice", position: "The Important One" },
  { id: 3, fname: "Bombasto", lname: "Nice", position: "A Dude" },
  { id: 4, fname: "Celeritas", lname: "Nice", position: "" },
  { id: 5, fname: "Magneta", lname: "Nice", position: "" },
  { id: 6, fname: "RubberMan", lname: "Nice", position: "" },
  { id: 7, fname: "Dynama", lname: "Nice", position: "" },
  { id: 8, fname: "Dr.", lname: "Nice", position: "" },
  { id: 9, fname: "Magma", lname: "Nice", position: "" },
  { id: 10, fname: "Tornado", lname: "Nice", position: "" },
];
