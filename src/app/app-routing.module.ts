import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";

import { JobsComponent } from "./jobs/jobs.component";
import { JobAddComponent } from "./jobs/job-add/add.component";
import { JobDetailComponent } from "./jobs/job-detail/job-detail.component";
import { JobEditComponent } from "./jobs/job-edit/job-edit.component";

import { TestsComponent } from "./tests/tests.component";
import { TestDetailComponent } from "./tests/test-detail/test-detail.component";
import { TestAddComponent } from "./tests/test-add/test-add.component";
import { TestEditComponent } from "./tests/test-edit/test-edit.component";

import { PersonsComponent } from "./persons/persons.component";
import { PersonDetailComponent } from "./persons/person-detail/person-detail.component";
import { PersonAddComponent } from "./persons/person-add/person-add.component";
import { PersonEditComponent } from "./persons/person-edit/person-edit.component";

import { CalendarComponent } from "./calendar/calendar.component";
import { DocumentsComponent } from "./documents/documents.component";
import { SettingsComponent } from "./settings/settings.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "tests", component: TestsComponent },
  { path: "test/add", component: TestAddComponent },
  { path: "test/:id", component: TestDetailComponent },
  { path: "test/:id/edit", component: TestEditComponent },
  { path: "jobs", component: JobsComponent },
  { path: "job/:id", component: JobDetailComponent },
  { path: "job/:id/edit", component: JobEditComponent },
  { path: "job/add", component: JobAddComponent },
  { path: "documents", component: DocumentsComponent },
  { path: "settings", component: SettingsComponent },
  { path: "persons", component: PersonsComponent },
  { path: "person/add", component: PersonAddComponent },
  { path: "person/:id", component: PersonDetailComponent },
  { path: "person/:id/edit", component: PersonEditComponent },
  { path: "calendar", component: CalendarComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
