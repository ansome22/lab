import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { HomeComponent } from "./home/home.component";
import { TestsComponent } from "./tests/tests.component";
import { JobsComponent } from "./jobs/jobs.component";
import { DocumentsComponent } from "./documents/documents.component";
import { SettingsComponent } from "./settings/settings.component";
import { JobAddComponent } from "./jobs/job-add/add.component";
import { TestAddComponent } from "./tests/test-add/test-add.component";
import { PersonsComponent } from "./persons/persons.component";
import { PersonAddComponent } from "./persons/person-add/person-add.component";
import { MessagesComponent } from "./messages/messages.component";
import { CalendarComponent } from "./calendar/calendar.component";

import { MaterialModule } from "./material";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { JobDetailComponent } from './jobs/job-detail/job-detail.component';
import { PersonDetailComponent } from './persons/person-detail/person-detail.component';
import { TestDetailComponent } from './tests/test-detail/test-detail.component';
import { JobEditComponent } from './jobs/job-edit/job-edit.component';
import { PersonEditComponent } from './persons/person-edit/person-edit.component';
import { TestEditComponent } from './tests/test-edit/test-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    TestsComponent,
    JobsComponent,
    HomeComponent,
    DocumentsComponent,
    SettingsComponent,
    JobAddComponent,
    TestAddComponent,
    PersonsComponent,
    PersonAddComponent,
    MessagesComponent,
    CalendarComponent,
    JobDetailComponent,
    PersonDetailComponent,
    TestDetailComponent,
    JobEditComponent,
    PersonEditComponent,
    TestEditComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, BrowserAnimationsModule, MaterialModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
