import { Component, OnInit } from "@angular/core";
import { CalendarEvent } from "./calendar";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.scss"],
})
export class CalendarComponent implements OnInit {
  viewDate: Date = new Date();

  firstDay = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth(), 1);
  currentDay = this.viewDate.getDate();
  lastDay = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth() + 1, 0);

  numbers = Array.from(new Array(this.lastDay.getDate()), (val, index) => index + 1);

  events: CalendarEvent[] = [
    {
      id: 1,
      start: new Date("2019-01-08"),
      end: new Date("2016-01-10"),
      title: "One day excluded event",
      color: "",
      allDay: true,
    },
    {
      id: 2,
      start: new Date("2019-013-01"),
      end: new Date("2016-01-09"),
      title: "Multiple weeks event",
      color: "",
      allDay: true,
    },
  ];

  constructor() {}

  ngOnInit() {}
}
